/*
 * Screen.h
 *
 *  Created on: Oct 28, 2018
 *      Author: christoph
 */

#ifndef SCREEN_H_
#define SCREEN_H_

#include <SDL.h>
#include <tuple>

namespace cw {

struct ScreenSize {
	int width;
	int height;
};

class Screen {
private:
	SDL_Window *m_window;
	SDL_Renderer *m_renderer;
    SDL_Texture *m_texture;
    Uint32 *m_buffer;

public:
	const ScreenSize SCREEN_SIZE;
	Screen(ScreenSize);
	virtual ~Screen();
	Uint32* getBuffer(){return m_buffer;};
	void setPixel(int x_pos, int y_pos, Uint8 red, Uint8 green, Uint8 blue);
	void drawBuffer();
	bool init();
	void close();
};

} /* namespace cw */

#endif /* SCREEN_H_ */
