//============================================================================
// Name        : MandelbrotSet.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <complex>
#include <cmath>
#include "MandelbrotIterator.h"
#include "Screen.h"
using namespace std;

struct color {
	Uint8 red;
	Uint8 green;
	Uint8 blue;
};

color colorbar(complex<float> comp_num) {
	//Uint8 r = (Uint8) (0);

	if (isnan(real(comp_num)) or isnan(imag(comp_num))){
		return color {255, 255, 255};
	}

	//Uint8 r = (Uint8) (abs(comp_num) / 2. * 255);
	Uint8 r = (Uint8) (arg(comp_num) / M_PI * 128.);
	Uint8 g = (Uint8) (real(comp_num) / 2. * 128. + 128.);
	Uint8 b = (Uint8) (imag(comp_num) / 2. * 100 + 128);
	return color { r, g, b };
}

int main() {

	const int REAL_N_PIXELS = 1600;
	const int IMAG_N_PIXELS = 800;

	MandelbrotIterator M_iterator(REAL_N_PIXELS, IMAG_N_PIXELS);
	cw::Screen screen(cw::ScreenSize { REAL_N_PIXELS, IMAG_N_PIXELS });
	screen.init();

	screen.drawBuffer();


	SDL_Event event;
	bool quit = false;
	bool iterate = true;
	while (!quit) {
		//game loop
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				quit = true;
				cout << "Quit event thrown" << endl;
			}
			else if (event.type == SDL_KEYDOWN) {
				cout << "Toggle iteration" << endl;
				iterate = !iterate;
			}
		}

		if (iterate) {
			// fill display with numbers from iterator
			color c;
			const complex<float>* VALUES = M_iterator.getMValues();
			const int * ITER = M_iterator.getIterValues();
			int total_iter = M_iterator.getNIterations();
			Uint8 color_value;
			for (int i = 0; i < IMAG_N_PIXELS; i++) {
				for (int j = 0; j < REAL_N_PIXELS; j++) {
					if (ITER[i * REAL_N_PIXELS + j] == 0){
						c = colorbar(VALUES[i * REAL_N_PIXELS + j]);
						screen.setPixel(j, i, c.red, c.green, c.blue);
					}
					else {
						color_value = (Uint8)(((float)ITER[i * REAL_N_PIXELS + j]/(float)total_iter) * 255);
						screen.setPixel(j, i, color_value, color_value, color_value);
					}
				}
			}
			screen.drawBuffer();

			M_iterator.iterate(1);
		}
		else{
			SDL_Delay(10);
		}

		//screen.drawBuffer();
	}


	return 0;

}
