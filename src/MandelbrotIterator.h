/*
 * MandelbrotIterator.h
 *
 *  Created on: Nov 14, 2018
 *      Author: christoph
 */

#ifndef MANDELBROTITERATOR_H_
#define MANDELBROTITERATOR_H_

#include <vector>
#include <complex>

class MandelbrotIterator {
private:
	int n_iterations = 0;
	std::complex<float>* m_values;
	int* m_n_diverge;
	std::vector<float> m_real_values;
	std::vector<float> m_imag_values;


public:
	const int REAL_N_PIXELS;
	const int IMAG_N_PIXELS;
	const float REAL_LOW = -2.;
	const float REAL_HIGH = 1.;
	const float IMAG_LOW = -1.;
	const float IMAG_HIGH = 1.;

private:
	void iterateOnce();

public:
	MandelbrotIterator(int real_n_pixels, int imag_n_pixels);
	virtual ~MandelbrotIterator();
	void iterate(int n_iterations);
	void printCurrentMatrix();
	const std::complex<float>* getMValues();
	const int* getIterValues();
	int getNIterations();

};

#endif /* MANDELBROTITERATOR_H_ */
