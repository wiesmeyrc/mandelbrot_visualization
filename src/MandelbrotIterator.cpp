/*
 * MandelbrotIterator.cpp
 *
 *  Created on: Nov 14, 2018
 *      Author: christoph
 */

#include <vector>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <complex>
#include "MandelbrotIterator.h"
#include "aux.h"

using namespace std;
using namespace std::literals;

MandelbrotIterator::MandelbrotIterator(int real_n_pixels, int imag_n_pixels) :
		REAL_N_PIXELS(real_n_pixels), IMAG_N_PIXELS(imag_n_pixels) {

	std::cout << "Starting MIterator constructor" << std::endl;

	// allocate the arrays storing the iterator values, the real and imaginary lut
	m_values = new std::complex<float>[REAL_N_PIXELS * IMAG_N_PIXELS];
	m_n_diverge = new int [REAL_N_PIXELS * IMAG_N_PIXELS];
	m_real_values.reserve(REAL_N_PIXELS);
	m_imag_values.reserve(IMAG_N_PIXELS);

	// initialize the real and imaginary part vectors
	for (int i = 0; i < REAL_N_PIXELS; i++) {
		m_real_values[i] = REAL_LOW
				+ (float) i * (REAL_HIGH - REAL_LOW)
						/ ((float) REAL_N_PIXELS - 1);
		//cout << "real value " << i << " = " << m_real_values[i] << endl;
	}
	for (int i = 0; i < IMAG_N_PIXELS; i++) {
		m_imag_values[i] = IMAG_LOW
				+ (float) i * (IMAG_HIGH - IMAG_LOW)
						/ ((float) IMAG_N_PIXELS - 1);
		//cout << "imag value " << i << " = " << m_imag_values[i] << endl;
	}

	// initialize m_values matrix for iteration
	for (int i = 0; i < IMAG_N_PIXELS; i++) {
		for (int j = 0; j < REAL_N_PIXELS; j++) {
			m_values[i*REAL_N_PIXELS+j] = complex<float>(m_real_values[j], m_imag_values[i]);
		}
	}

	// initialize divergent counter
	memset(m_n_diverge, 0, REAL_N_PIXELS * IMAG_N_PIXELS*sizeof(int));

}

MandelbrotIterator::~MandelbrotIterator() {
	// TODO Auto-generated destructor stub
	std::cout << "Starting MIterator destructor" << std::endl;

	delete[] m_values;
	delete[] m_n_diverge;
}

void MandelbrotIterator::iterateOnce(){
	// perform one iteration
	n_iterations ++;
	for (int i = 0; i < IMAG_N_PIXELS; i++) {
		for (int j = 0; j < REAL_N_PIXELS; j++) {
			complex<float> current_value = m_values[i*REAL_N_PIXELS+j];
			current_value = current_value*current_value + complex<float>(m_real_values[j], m_imag_values[i]);
			if (abs(current_value) > 2.) {
				current_value = complex<float>(NAN, NAN);

				if (m_n_diverge[i*REAL_N_PIXELS+j] == 0) {
					m_n_diverge[i*REAL_N_PIXELS+j] = n_iterations;
				}
			}
			m_values[i*REAL_N_PIXELS+j] = current_value;
		}
	}

}

void MandelbrotIterator::iterate(int n_iterations){
	for (int i=0; i<n_iterations; i++){
		iterateOnce();
	}
}

void MandelbrotIterator::printCurrentMatrix(){
	// cout the current matrix iteration

	if (IMAG_N_PIXELS > 20){
		cout << "Matrix too big to display, exiting." << endl;
		return;
	}
	if (REAL_N_PIXELS > 20){
		cout << "Matrix too big to display, exiting." << endl;
		return;
	}

	cout << "Printing absolute values of matrix after " << n_iterations << " iterations." << endl;
	cout << fixed;
	cout.precision(2);
	for (int i = 0; i < IMAG_N_PIXELS; i++) {
		for (int j = 0; j < REAL_N_PIXELS; j++) {
			cout << setw(13) << m_values[i*REAL_N_PIXELS+j] << "  ";
		}
		cout << endl;
	}
}

const std::complex<float>* MandelbrotIterator::getMValues(){
	return m_values;
}

const int* MandelbrotIterator::getIterValues(){
	return m_n_diverge;
}

int MandelbrotIterator::getNIterations(){
	return n_iterations;
}
