/*
 * Screen.cpp
 *
 *  Created on: Oct 28, 2018
 *      Author: christoph
 */

#include <iostream>
#include <SDL.h>
#include "Screen.h"

using namespace std;

namespace cw {

Screen::Screen(ScreenSize size): m_window(NULL), m_renderer(NULL), m_texture(NULL), m_buffer(NULL), SCREEN_SIZE(size) {
	// TODO Auto-generated constructor stub
}

bool Screen::init(){

	m_window = SDL_CreateWindow("SDL Window",
			SDL_WINDOWPOS_UNDEFINED,           // initial x position
	        SDL_WINDOWPOS_UNDEFINED,           // initial y position
	        SCREEN_SIZE.width,                               // width, in pixels
	        SCREEN_SIZE.height,                               // width, in pixels
			SDL_WINDOW_SHOWN);

	if (m_window == NULL){
		SDL_Quit();
		cout << "Could not create window" << endl;

		return false;
	}

	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawBlendMode(m_renderer, SDL_BLENDMODE_BLEND);

	if (m_renderer == NULL){
		SDL_Quit();
		cout << "Could not create renderer" << endl;

		return false;
	}

	m_texture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC,
    		                      SCREEN_SIZE.width, SCREEN_SIZE.height);

	if (m_texture == NULL){
		SDL_Quit();
		cout << "Could not create texture" << endl;

		return false;
	}

	m_buffer = new Uint32[SCREEN_SIZE.width * SCREEN_SIZE.height];
	memset(m_buffer, 0, SCREEN_SIZE.width * SCREEN_SIZE.height*sizeof(Uint32));

	return true;
}

Screen::~Screen() {
	// TODO Auto-generated destructor stub
    SDL_DestroyTexture(m_texture);
    SDL_DestroyRenderer(m_renderer);
	SDL_DestroyWindow(m_window);
	delete [] m_buffer;
	cout << "Cleaned everything up" << endl;
}

void Screen::setPixel(int x_pos, int y_pos, Uint8 red, Uint8 green, Uint8 blue){

	Uint32 color = 0;

	color += red;
	color <<= 8;
	color += green;
	color <<= 8;
	color += blue;
	color <<= 8;

	m_buffer[y_pos*SCREEN_SIZE.width + x_pos] = color;
}

void Screen::drawBuffer(){
	SDL_UpdateTexture(m_texture, NULL, m_buffer, SCREEN_SIZE.width*sizeof(Uint32));
	SDL_RenderClear(m_renderer);
	SDL_RenderCopy(m_renderer, m_texture, NULL, NULL);
	SDL_RenderPresent(m_renderer);
}

} /* namespace cw */
